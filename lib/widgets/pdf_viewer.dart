import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/widgets/baden_bottom_sheet.dart';
import 'package:cvjm_baden_app/widgets/block_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:flutter_cached_pdfview/src/url/custom_cache_manger.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:share_plus/share_plus.dart';

class _DownloadIndicator extends StatelessWidget {
  final double progress;

  const _DownloadIndicator({required this.progress});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircularProgressIndicator(value: progress / 100),
          const SizedBox(height: BadenLayout.defaultPadding),
          Text(AppLocalizations.of(context)!.downloading),
        ],
      ),
    );
  }
}

void _saveFile(BuildContext context, String url, String fileName) async {
  final file = await CustomCacheManger(
    key: "libCachedPdfView",
    maxAgeCacheObject: const Duration(days: 10),
    maxNrOfCacheObjects: 100,
  ).getSingleFile(url);

  final pickedDir = await FlutterFileDialog.pickDirectory();
  if (pickedDir == null) return;
  await FlutterFileDialog.saveFileToDirectory(
    directory: pickedDir,
    data: file.readAsBytesSync(),
    fileName: fileName,
    mimeType: "application/pdf",
  );

  if (!context.mounted) return;

  showModalBottomSheet(
    constraints: const BoxConstraints(maxWidth: 500),
    context: context,
    builder: (context) => BadenBottomSheet(
      text: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: AppLocalizations.of(context)!.fileHasBeenSaved,
          style: TextStyle(
            color: Theme.of(context).textTheme.bodyMedium?.color,
            fontFamily: Theme.of(context).textTheme.bodyMedium?.fontFamily,
          ),
        ),
      ),
      children: [
        BlockButton(
          onTap: () => Navigator.pop(context),
          label: AppLocalizations.of(context)!.ok,
          child: const Icon(Symbols.done, size: 40),
        ),
      ],
    ),
  );
}

Widget _downloadIndicator(double progress) =>
    _DownloadIndicator(progress: progress);

class PdfViewer extends StatelessWidget {
  final String url;

  const PdfViewer({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final fileName = url.split("/").last;
    return Scaffold(
      appBar: AppBar(
        title: Text(fileName),
        backgroundColor: Colors.white,
        actions: [
          FutureBuilder(
              future: FlutterFileDialog.isPickDirectorySupported(),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return const SizedBox();
                }
                if (!snapshot.data!) return const SizedBox();

                return IconButton(
                  onPressed: () => _saveFile(context, url, fileName),
                  icon: const Icon(Symbols.save),
                );
              }),
          IconButton(
            onPressed: () async {
              final file = await CustomCacheManger(
                key: "libCachedPdfView",
                maxAgeCacheObject: const Duration(days: 10),
                maxNrOfCacheObjects: 100,
              ).getSingleFile(url);
              Share.shareXFiles([XFile(file.path)]);
            },
            icon: const Icon(Symbols.share),
          ),
          const SizedBox(width: BadenLayout.defaultPadding),
        ],
      ),
      body: const PDF().cachedFromUrl(
        url,
        placeholder: _downloadIndicator,
      ),
    );
  }
}
