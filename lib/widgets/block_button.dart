import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:flutter/material.dart';

/// A Button primarily used in bottomsheets
class BlockButton extends StatelessWidget {
  final double? width;
  final double? height;

  final Color? backgroundColor;

  final BorderRadiusGeometry? borderRadius;

  final Function() onTap;

  final String label;

  final Widget child;

  const BlockButton(
      {Key? key,
      this.width,
      this.height,
      this.borderRadius,
      required this.onTap,
      required this.label,
      required this.child,
      this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? 100,
      height: height ?? 100,
      child: ClipRRect(
        borderRadius: borderRadius ??
            BorderRadius.circular(BadenLayout.defaultBorderRadius),
        child: Material(
          child: Ink(
            color: backgroundColor ??
                Theme.of(context).colorScheme.tertiaryContainer,
            child: InkWell(
              onTap: onTap,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    child,
                    Text(
                      label,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
