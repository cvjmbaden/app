import 'dart:io';

import 'package:cvjm_baden_app/js_handler/js_handler.dart';
import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:cvjm_baden_app/no_internet.dart';
import 'package:cvjm_baden_app/widgets/baden_bottom_sheet.dart';
import 'package:cvjm_baden_app/widgets/block_button.dart';
import 'package:cvjm_baden_app/widgets/pdf_viewer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

const allowedHosts = [
  "www.cvjmbaden.de",
  "www.cvjm-marienhof.de",
  "www.schloss-unteroewisheim.de",
];

class BadenWebState extends ChangeNotifier {
  bool _canGoBack = false;

  bool get canGoBack => _canGoBack;

  set canGoBack(bool b) {
    print("CAN GO BACK: $b");
    _canGoBack = b;
    notifyListeners();
  }
}

class BadenWebViewController {
  BadenWebViewController._();

  static final webViewKey = GlobalKey();

  static InAppWebViewController? controller;
  static InAppWebViewGroupOptions groupOptions = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useOnDownloadStart: true,
      useShouldOverrideUrlLoading: true,
      supportZoom: false,
      userAgent: userAgent,
    ),
    ios: IOSInAppWebViewOptions(
      allowsInlineMediaPlayback: false,
    ),
  );

  static Future<void> loadUrl(String url) async {
    await controller?.loadUrl(
      urlRequest: URLRequest(
        url: Uri.parse(url),
        headers: {
          "FCM-Token": fcmToken ?? "",
        },
      ),
    );
  }
}

/// Shows a bottom sheet to ask whether to open a uri externally
void showOutOfPageNavigation(Uri uri, {BuildContext? context}) {
  showModalBottomSheet(
    constraints: const BoxConstraints(maxWidth: 500),
    context: context ?? BadenApp.navigationKey.currentContext!,
    builder: (context) => BadenBottomSheet(
      text: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: "${AppLocalizations.of(context)!.linkToExternalWebsite}:\n",
          style: TextStyle(
            color: Theme.of(context).textTheme.bodyMedium?.color,
            fontFamily: Theme.of(context).textTheme.bodyMedium?.fontFamily,
          ),
          children: [
            TextSpan(
              text: uri.host,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ],
        ),
      ),
      children: [
        BlockButton(
          onTap: () {
            launchUrl(uri, mode: LaunchMode.externalApplication);
            Navigator.pop(context);
          },
          label: AppLocalizations.of(context)!.openInBrowser,
          child: const Icon(Symbols.language, size: 40),
        ),
        BlockButton(
          onTap: () => Navigator.pop(context),
          label: AppLocalizations.of(context)!.cancel,
          child: const Icon(Symbols.cancel, size: 40),
        ),
      ],
    ),
  );
}

class BadenWebViewWidget extends StatefulWidget {
  const BadenWebViewWidget({Key? key}) : super(key: key);

  @override
  State<BadenWebViewWidget> createState() => _BadenWebViewWidgetState();
}

/// Handles messages interaction on
/// a. App opening by notification click
void firebaseHandleMessageInteraction(RemoteMessage message) {
  if (BadenWebViewController.controller == null) return;

  if (message.data["link"] != null) {
    BadenWebViewController.loadUrl(message.data["link"]);
  }
}

class _BadenWebViewWidgetState extends State<BadenWebViewWidget> {
  @override
  void initState() {
    super.initState();
  }

  void onDownloadRequest(
      InAppWebViewController controller, DownloadStartRequest request) {
    final fileNameWithExt = request.url.pathSegments.last.split(".");
    if (fileNameWithExt.length == 1 || fileNameWithExt.last != "pdf") {
      // no extension found or no pdf
      launchUrl(request.url, mode: LaunchMode.externalApplication);
      return;
    }

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PdfViewer(url: request.url.toString()),
      ),
    );
  }

  Future<NavigationActionPolicy> shouldCancelNavigation(
      InAppWebViewController controller,
      NavigationAction navigationAction) async {
    if (!navigationAction.isForMainFrame) return NavigationActionPolicy.ALLOW;

    final uri = navigationAction.request.url!;

    if (BadenApp.drawerConfig == null) return NavigationActionPolicy.CANCEL;

    List<String> allowed = BadenApp.drawerConfig!.allowedHosts ?? allowedHosts;
    if (!allowed.contains(uri.host)) {
      // Disables out-of-page navigation.
      showOutOfPageNavigation(navigationAction.request.url!);
      return NavigationActionPolicy.CANCEL;
    }

    // disallow in order to set headers
    if (Platform.isAndroid ||
        navigationAction.iosWKNavigationType ==
            IOSWKNavigationType.LINK_ACTIVATED) {
      navigationAction.request.headers ??= {};
      if (fcmToken != null) {
        navigationAction.request.headers!["FCM-Token"] = fcmToken!;
      }

      controller.loadUrl(urlRequest: navigationAction.request);
      return NavigationActionPolicy.CANCEL;
    }

    return NavigationActionPolicy.ALLOW;
  }

  static const _errorCodesForNoInternet = [
    // Android
    // https://developer.android.com/reference/android/webkit/WebViewClient
    -2,
    -6,
    -8,

    // apple
    // https://developer.apple.com/documentation/foundation/1508628-url_loading_system_error_codes/
    -1003,
    -2000,
    -1020,
    -1006,
    -1005,
    -1004,
    -1009,
    -1001,
    -999,
  ];

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.passthrough,
      children: [
        InAppWebView(
          key: BadenWebViewController.webViewKey,
          onTitleChanged: (controller, title) {
            BadenApp.navTitleState.currentTitle = title ?? "Home";
          },
          onLoadError: (controller, url, code, message) {
            if (_errorCodesForNoInternet.contains(code)) {
              BadenApp.navigationKey.currentState!.push(
                MaterialPageRoute(builder: (context) => const NoInternetHome()),
              );
            }
          },
          initialUrlRequest: URLRequest(
            url: Uri.parse(BadenApp.drawerConfig!.initialUrl),
            headers: {
              "FCM-Token": fcmToken ?? "",
            },
          ),
          initialOptions: BadenWebViewController.groupOptions,
          onWebViewCreated: (controller) {
            registerJSHandlers(controller);
            BadenWebViewController.controller = controller;
          },
          androidOnPermissionRequest: (controller, origin, resources) async =>
              PermissionRequestResponse(
            resources: resources,
            action: PermissionRequestResponseAction.GRANT,
          ),
          shouldOverrideUrlLoading: shouldCancelNavigation,
          onLoadStop: (controller, url) async {
            controller
                .canGoBack()
                .then((value) => BadenApp.controllerState.canGoBack = value);
            BadenApp.navTitleState.currentTitle =
                await controller.getTitle() ?? "Home";
          },
          onDownloadStartRequest: onDownloadRequest,
          onPageCommitVisible: (controller, url) async {
            final initialMessage =
                await FirebaseMessaging.instance.getInitialMessage();
            if (initialMessage != null) {
              firebaseHandleMessageInteraction(initialMessage);
            }
          },
        ),
        Consumer<BadenWebState>(
          builder: (context, value, child) => _BackButton(
            visible: value.canGoBack,
          ),
        ),
      ],
    );
  }
}

class _BackButton extends StatelessWidget {
  final bool visible;

  const _BackButton({Key? key, required this.visible}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: BadenLayout.defaultPadding * 1.5,
      right: BadenLayout.defaultPadding * 1.5,
      child: Visibility(
        visible: visible,
        child: FloatingActionButton(
          backgroundColor: Colors.white,
          onPressed: () async =>
              await BadenWebViewController.controller?.goBack(),
          elevation: 0,
          shape: const CircleBorder(side: BorderSide()),
          child: const Icon(Symbols.arrow_back_rounded, size: 30),
        ),
      ),
    );
  }
}
