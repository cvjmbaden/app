import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:flutter/material.dart';

/// A basic template class for displaying layout-consistent bottomsheets
class BadenBottomSheet extends StatelessWidget {
  final Widget text;
  final List<Widget> children;

  const BadenBottomSheet({Key? key, required this.text, required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(BadenLayout.defaultPadding),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          text,
          const SizedBox(height: BadenLayout.defaultPadding),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: children,
          ),
        ],
      ),
    );
  }
}
