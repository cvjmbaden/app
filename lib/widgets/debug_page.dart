import 'package:cvjm_baden_app/drawer/app_config.dart';
import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_symbols_icons/symbols.dart';

class DebugPage extends StatefulWidget {
  const DebugPage({Key? key}) : super(key: key);

  @override
  State<DebugPage> createState() => _DebugPageState();
}

class _DebugPageState extends State<DebugPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Debug"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: BadenLayout.defaultPadding,
        ),
        child: ListView(
          children: [
            Text(
              "General information",
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            ListTile(
              leading: const Icon(Symbols.backup_table),
              title: Text(versionString),
              subtitle: const Text("App Version"),
            ),
            const ListTile(
              leading: Icon(Symbols.menu),
              title: Text("$configVersion"),
              subtitle: Text("Config Version"),
            ),
            FutureBuilder(
              future: FirebaseMessaging.instance.getToken(),
              builder: (context, snapshot) {
                return ListTile(
                  leading: const Icon(Symbols.notifications),
                  trailing: snapshot.connectionState == ConnectionState.done
                      ? IconButton(
                          onPressed: () {
                            Clipboard.setData(
                                ClipboardData(text: snapshot.data ?? ""));
                            Fluttertoast.showToast(msg: "Copied FCM token");
                          },
                          icon: const Icon(Symbols.content_copy),
                        )
                      : null,
                  title: Text(
                    snapshot.connectionState == ConnectionState.done
                        ? snapshot.data ?? "Couldn't load token"
                        : "Loading...",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  subtitle: const Text("Firebase Messaging Token"),
                );
              },
            ),
            const SizedBox(height: BadenLayout.defaultPadding),
            Text(
              "Notifications",
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            const Text(
              "Subscribed Topics",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
