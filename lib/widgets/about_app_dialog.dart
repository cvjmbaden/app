import 'package:cvjm_baden_app/generated/assets.dart';
import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// A custom [AboutDialog]
class AboutAppDialog extends StatelessWidget {
  const AboutAppDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);

    final MaterialLocalizations localizations =
        MaterialLocalizations.of(context);

    const name = "CVJM Baden App";
    const legals = "© CVJM Landesverband Baden e.V.";

    return AlertDialog(
      icon: const Icon(Symbols.info),
      elevation: 0,
      content: ListBody(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(name, style: themeData.textTheme.headlineSmall),
                      Text(versionString,
                          style: themeData.textTheme.bodyMedium),
                      const SizedBox(height: BadenLayout.defaultPadding),
                      Text(legals, style: themeData.textTheme.bodySmall),

                      const SizedBox(height: BadenLayout.defaultPadding),
                      // Buttons
                      TextButton(
                        child:
                            Text(AppLocalizations.of(context)!.privacy_policy),
                        onPressed: () {
                          BadenWebViewController.loadUrl(
                            BadenApp.drawerConfig!.privacyPolicyLink,
                          );
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                      ),
                      TextButton(
                        child: Text(AppLocalizations.of(context)!.imprint),
                        onPressed: () {
                          BadenWebViewController.loadUrl(
                            BadenApp.drawerConfig!.imprintLink,
                          );
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                      ),
                      TextButton(
                        child: Text(localizations.viewLicensesButtonLabel),
                        onPressed: () {
                          showLicensePage(
                            context: context,
                            applicationName: name,
                            applicationVersion: versionString,
                            applicationIcon: SvgPicture.asset(
                              Assets.graphicsLogo,
                              width: 100,
                            ),
                            applicationLegalese: legals,
                          );
                        },
                      ),
                      TextButton(
                        child: Text(localizations.closeButtonLabel),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
      scrollable: true,
    );
  }
}
