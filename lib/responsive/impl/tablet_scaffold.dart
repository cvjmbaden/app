import 'package:cvjm_baden_app/drawer/baden_drawer.dart';
import 'package:cvjm_baden_app/responsive/responsive_scaffold.dart';
import 'package:cvjm_baden_app/utils.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Builds the scaffold for tablets.
class TabletScaffold extends StatelessWidget {
  const TabletScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        surfaceTintColor: Colors.white,
        title: Consumer<NavigationTitleState>(
          builder: (context, value, child) => Text(
            value.currentTitle.replaceStart("CVJM Baden - ", ""),
          ),
        ),
      ),
      drawer: const BadenDrawer(),
      backgroundColor: Colors.white,
      body: const BadenWebViewWidget(),
    );
  }
}
