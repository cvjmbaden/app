import 'package:flutter/material.dart';

const _tabletWidth = 500;
const _desktopWidth = 1200;

/// Responsible for updating the appbar's title
class NavigationTitleState extends ChangeNotifier {
  String _currentTitle = "";

  String get currentTitle => _currentTitle;

  set currentTitle(String title) {
    if (title == "about:blank") title = "";
    _currentTitle = title;
    notifyListeners();
  }
}

/// A Responsive scaffold selector based on screen width.
class ResponsiveScaffold extends StatelessWidget {
  final Widget mobileScaffold;
  final Widget tabletScaffold;
  final Widget desktopScaffold;

  const ResponsiveScaffold({
    Key? key,
    required this.mobileScaffold,
    required this.tabletScaffold,
    required this.desktopScaffold,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < _tabletWidth) return mobileScaffold;
        if (constraints.maxWidth < _desktopWidth) return tabletScaffold;
        return desktopScaffold;
      },
    );
  }
}
