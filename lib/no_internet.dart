import 'package:cvjm_baden_app/drawer/app_config.dart';
import 'package:cvjm_baden_app/generated/assets.dart';
import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/notifications/firebase.dart';
import 'package:cvjm_baden_app/utils.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:cvjm_baden_app/widgets/stroked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:nice_loading_button/nice_loading_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:http/http.dart' as http;

/// Widget for displaying a can't connect to internet screen.
class NoInternetHome extends StatefulWidget {
  const NoInternetHome({Key? key}) : super(key: key);

  @override
  State<NoInternetHome> createState() => _NoInternetHomeState();
}

/// State management, building the Widget.
class _NoInternetHomeState extends State<NoInternetHome> {
  bool tryConnect = false;

  void showErrorToast() {
    Fluttertoast.cancel().then(
      (value) => Fluttertoast.showToast(
        msg: AppLocalizations.of(context)!.couldntConnectToInternet,
      ),
    );
  }

  /// Handles the click on the "Reload" button
  void onReloadButtonClick(startLoading, stopLoading, btnState) async {
    if (btnState == ButtonState.busy || tryConnect) return;
    startLoading();
    setState(() => tryConnect = true);

    http.Response res;

    try {
      res = await http.get(Uri.parse(configUrl));
    } catch (e) {
      showErrorToast();
      stopLoading();
      setState(() {
        tryConnect = false;
      });
      return;
    }

    await loadConfig(res.body);

    BadenWebViewController.controller!.reload();
    if (mounted) {
      Navigator.pop(context);
    }

    tryFCMInitialization();

    setState(() {
      tryConnect = false;
    });
    stopLoading();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, // disable manual popping
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Positioned(
              bottom: 0,
              right: 0,
              child: SvgPicture.asset(
                Assets.graphicsDreiecke,
                width: 250,
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(BadenLayout.defaultPadding * 2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    // WiFi icon.
                    const Expanded(
                      child: Center(
                        child: Icon(
                          Symbols.wifi_off_rounded,
                          size: 150,
                          color: Colors.black26,
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Headline text.
                        StrokedText(
                          AppLocalizations.of(context)!.noConnectionPossible,
                          style: Theme.of(context)
                              .textTheme
                              .headlineLarge
                              ?.copyWith(
                                fontWeight: FontWeight.w100,
                              ),
                          strokeWidth: 10,
                          strokeColor: Colors.white,
                        ),
                        const SizedBox(height: BadenLayout.defaultPadding),
                        StrokedText(
                          AppLocalizations.of(context)!.noConnectionInformation,
                          style: Theme.of(context).textTheme.bodyLarge,
                          strokeWidth: 10,
                          strokeColor: Colors.white,
                        ),
                        const SizedBox(height: BadenLayout.defaultPadding),
                        IgnorePointer(
                          ignoring: tryConnect,
                          child: LoadingButton(
                            width: calcTextSize(
                                  context,
                                  AppLocalizations.of(context)!.tryAgain,
                                  Theme.of(context).textTheme.bodyMedium!,
                                ).width +
                                2 * BadenLayout.defaultPadding,
                            height: 50,
                            elevation: 0,
                            animate: true,
                            borderRadius: BadenLayout.defaultBorderRadius,
                            borderSide: const BorderSide(width: 1.5),
                            color: Colors.transparent,
                            onTap: onReloadButtonClick,
                            loader: Container(
                              padding: const EdgeInsets.all(10),
                              width: 40,
                              height: 40,
                              child: const CircularProgressIndicator(
                                color: Colors.black,
                              ),
                            ),
                            child: Text(
                              AppLocalizations.of(context)!.tryAgain,
                              style: const TextStyle(color: Colors.black),
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Empty box for layout purposes (centering)
                    const Expanded(child: SizedBox()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
