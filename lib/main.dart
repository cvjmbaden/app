import 'dart:io';

import 'package:cvjm_baden_app/colors.dart';
import 'package:cvjm_baden_app/drawer/app_config.dart';
import 'package:cvjm_baden_app/generated/assets.dart';
import 'package:cvjm_baden_app/notifications/firebase.dart';
import 'package:cvjm_baden_app/responsive/impl/desktop_scaffold.dart';
import 'package:cvjm_baden_app/responsive/impl/mobile_scaffold.dart';
import 'package:cvjm_baden_app/responsive/impl/tablet_scaffold.dart';
import 'package:cvjm_baden_app/responsive/responsive_scaffold.dart';
import 'package:cvjm_baden_app/utils.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

late final String versionString;
String? fcmToken;
late final String userAgent;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  PackageInfo info = await PackageInfo.fromPlatform();
  versionString = "Version ${info.version} Build ${info.buildNumber}";
  userAgent = "CVJMBadenApp/${info.version}+${info.buildNumber}";

  // Let's encrypt cert
  ByteData data = await PlatformAssetBundle().load(Assets.caLetsEncryptR3);
  SecurityContext.defaultContext
      .setTrustedCertificatesBytes(data.buffer.asUint8List());

  await Hive.initFlutter();

  await tryFCMInitialization();
  await loadConfigFromServer();

  // Adding font licenses.
  LicenseRegistry.addLicense(() async* {
    final silOpenFont = await rootBundle.loadString(Assets.licensesSILOfl);
    yield LicenseEntryWithLineBreaks(["SourceSansPro"], silOpenFont);
  });

  runApp(const BadenApp());
}

class BadenApp extends StatefulWidget {
  static final navigationKey = GlobalKey<NavigatorState>();

  static AppConfig? drawerConfig;
  static final navTitleState = NavigationTitleState();
  static final controllerState = BadenWebState();

  const BadenApp({super.key});

  @override
  State<StatefulWidget> createState() => _BadenAppState();
}

class _BadenAppState extends State<BadenApp> with WidgetsBindingObserver {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      loadConfigFromServer();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  /// Builds the app
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: BadenApp.navTitleState),
        ChangeNotifierProvider.value(value: BadenApp.controllerState),
      ],
      child: MaterialApp(
        navigatorKey: BadenApp.navigationKey,
        title: "CVJM Baden",
        theme: ThemeData(
          // Font settings.
          fontFamily: "SourceSansPro",
          // Colors.
          colorScheme: lightColorScheme,
          // Everything else.
          useMaterial3: true,
        ),
        // Localization.
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        home: WillPopScope(
          onWillPop: () async {
            return BadenWebViewController.controller!
                .canGoBack()
                .then((value) async {
              if (value) {
                await BadenWebViewController.controller!.goBack();
                return false;
              }
              return true;
            });
          },
          child: const ResponsiveScaffold(
            mobileScaffold: MobileScaffold(),
            tabletScaffold: TabletScaffold(),
            desktopScaffold: DesktopScaffold(),
          ),
        ),
      ),
    );
  }
}
