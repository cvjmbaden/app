// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'icon_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IconConfig _$IconConfigFromJson(Map<String, dynamic> json) => IconConfig(
      codePoint: IconConfig._codepointFromJson(json['codePoint'] as Object),
      fill: (json['fill'] as num?)?.toDouble() ?? 0,
      weight: (json['weight'] as num?)?.toDouble() ?? 400,
      grade: (json['grade'] as num?)?.toDouble() ?? 0,
      style: $enumDecodeNullable(_$IconStyleEnumMap, json['style']) ??
          IconStyle.rounded,
    );

Map<String, dynamic> _$IconConfigToJson(IconConfig instance) {
  final val = <String, dynamic>{
    'codePoint': IconConfig._codepointToJson(instance.codePoint),
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('fill', instance.fill);
  writeNotNull('weight', instance.weight);
  writeNotNull('grade', instance.grade);
  writeNotNull('style', _$IconStyleEnumMap[instance.style]);
  return val;
}

const _$IconStyleEnumMap = {
  IconStyle.outlined: 'outlined',
  IconStyle.rounded: 'rounded',
  IconStyle.sharp: 'sharp',
};
