import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part "icon_config.g.dart";

enum IconStyle {
  outlined("MaterialSymbolsOutlined"),
  rounded("MaterialSymbolsRounded"),
  sharp("MaterialSymbolsSharp");

  final String fontFamily;
  const IconStyle(this.fontFamily);
}

/// Represents an icon json element
/// Configurable like on [this](https://fonts.google.com/icons?icon.set=Material+Symbols) website:
/// - Fill (1 or 0)
/// - Weight (100 - 700)
/// - Grade (-25, 0, 200)
@JsonSerializable(includeIfNull: false)
class IconConfig {
  /// The codepoint for the icon.
  /// List at: https://fonts.google.com/icons?icon.set=Material+Symbols.
  /// Getting codepoint:
  /// 1. Open [Symbol List](https://fonts.google.com/icons?icon.set=Material+Symbols).
  /// 2. Select desired symbol.
  /// 3. A sidepanel should appear.
  /// 4. Copy Code point.
  /// 5. Usage: `"0x{Code point}"` -> has to be a string, put 0x in front for marking as hex.
  @JsonKey(fromJson: _codepointFromJson, toJson: _codepointToJson)
  final int codePoint;

  /// Whether the icon is filled. (Default: 0)
  @JsonKey(defaultValue: 0)
  final double? fill;

  /// The stroke weight of the icon.
  /// Possible values: 100, 200, 300, 400 (default), 500, 600, 700.
  @JsonKey(defaultValue: 400)
  final double? weight;

  /// More granular control over the icon's thickness.
  /// Possible values: -25, 0 (default), 200.
  @JsonKey(defaultValue: 0)
  final double? grade;

  /// The icons style (rounded, outlined, sharp).
  @JsonKey(defaultValue: IconStyle.rounded)
  final IconStyle? style;

  const IconConfig(
      {required this.codePoint,
      this.fill,
      this.weight,
      this.grade,
      this.style});

  /// Builds an [IconConfig] from a JSON-like Map.
  factory IconConfig.fromJson(Map<String, dynamic> json) =>
      _$IconConfigFromJson(json);

  /// Builds a JSON-like Map representing this [IconConfig].
  Map<String, dynamic> toJson() => _$IconConfigToJson(this);

  /// Returns an [IconData] based on the [codePoint] and [style] for the icon.
  IconData get iconData {
    return IconData(codePoint,
        fontFamily: style?.fontFamily ?? IconStyle.rounded.fontFamily,
        fontPackage: "material_symbols_icons");
  }

  /// Returns an [Icon] based on the [iconData], [fill], [grade] and [weight].
  Icon get icon {
    return Icon(iconData, fill: fill, grade: grade, weight: weight);
  }

  /// Converts a [Object] to an [int].
  /// Used for decoding the [codePoint].
  static int _codepointFromJson(Object s) => int.parse(s.toString());

  /// Converts the [codePoint] to an hex-string.
  static String _codepointToJson(int codePoint) =>
      "0x${codePoint.toRadixString(16)}";
}
