import 'dart:developer';

import 'icon_config.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part "menu_element.g.dart";

@JsonEnum(valueField: "name")
enum MenuElementType {
  linkEntry("link_entry"),
  divider("divider"),
  externalLink("external_link"),
  aboutAppPopup("about_app_popup");

  final String name;

  const MenuElementType(this.name);
}

/// A single entry in the drawer.
@JsonSerializable(includeIfNull: false, explicitToJson: true)
class MenuElement {
  final MenuElementType type;

  final String? label;
  final IconConfig? icon;
  final IconConfig? selectedIcon;

  final String? path;

  const MenuElement({
    required this.type,
    this.label,
    this.icon,
    this.selectedIcon,
    this.path,
  });

  /// Creates a [MenuElement] from a Map
  factory MenuElement.fromJson(Map<String, dynamic> json) =>
      _$MenuElementFromJson(json);

  /// Creates the json-like representation as a [Map]
  Map<String, dynamic> toJson() => _$MenuElementToJson(this);

  bool validate() {
    if (type == MenuElementType.divider) return true;

    List<String> errors = [];
    if (label == null) errors.add("label");
    if (icon == null) errors.add("icon");

    if (type == MenuElementType.aboutAppPopup) return errors.isEmpty;
    if (path == null) errors.add("path");

    if (errors.isNotEmpty) {
      log("Error in menu element${label != null ? " $label" : ""}: ${errors.join(", ")} missing!");
    }
    return errors.isEmpty;
  }

  Widget buildWidget() {
    if (type == MenuElementType.divider) {
      return const Padding(
        padding: EdgeInsets.fromLTRB(28, 16, 28, 10),
        child: Divider(),
      );
    }
    return NavigationDrawerDestination(
      icon: icon!.icon,
      label: Text(label!),
      selectedIcon: selectedIcon?.icon,
    );
  }
}
