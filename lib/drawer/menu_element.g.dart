// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menu_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MenuElement _$MenuElementFromJson(Map<String, dynamic> json) => MenuElement(
      type: $enumDecode(_$MenuElementTypeEnumMap, json['type']),
      label: json['label'] as String?,
      icon: json['icon'] == null
          ? null
          : IconConfig.fromJson(json['icon'] as Map<String, dynamic>),
      selectedIcon: json['selectedIcon'] == null
          ? null
          : IconConfig.fromJson(json['selectedIcon'] as Map<String, dynamic>),
      path: json['path'] as String?,
    );

Map<String, dynamic> _$MenuElementToJson(MenuElement instance) {
  final val = <String, dynamic>{
    'type': _$MenuElementTypeEnumMap[instance.type]!,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('label', instance.label);
  writeNotNull('icon', instance.icon?.toJson());
  writeNotNull('selectedIcon', instance.selectedIcon?.toJson());
  writeNotNull('path', instance.path);
  return val;
}

const _$MenuElementTypeEnumMap = {
  MenuElementType.linkEntry: 'link_entry',
  MenuElementType.divider: 'divider',
  MenuElementType.externalLink: 'external_link',
  MenuElementType.aboutAppPopup: 'about_app_popup',
};
