import 'dart:developer';

import 'package:cvjm_baden_app/drawer/menu_element.dart';
import 'package:cvjm_baden_app/generated/assets.dart';
import 'package:cvjm_baden_app/layout/contants.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:cvjm_baden_app/widgets/about_app_dialog.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:cvjm_baden_app/widgets/debug_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_symbols_icons/symbols.dart';
import 'package:url_launcher/url_launcher.dart';

void showAboutApp(BuildContext context) {
  showDialog(
    context: BadenApp.navigationKey.currentContext!,
    builder: (ctx) => const AboutAppDialog(),
  );
}

/// Updates the webview based on the clicked icon.
void onDestinationSelected(BuildContext context, int index, bool pop) {
  late final MenuElement menuElement;
  try {
    menuElement = BadenApp.drawerConfig!.getMenuEntryWithoutDividers(index);
  } catch (e) {
    if (kDebugMode) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const DebugPage(),
        ),
      );
    } else {
      log("Cannot open menu element!");
      log(e.toString());
    }
    return;
  }

  // About app
  if (menuElement.type == MenuElementType.aboutAppPopup) {
    showAboutApp(context);
    return;
  }

  if (menuElement.type == MenuElementType.externalLink) {
    launchUrl(
      Uri.parse(menuElement.path!),
      mode: LaunchMode.externalApplication,
    );
    return;
  }

  BadenWebViewController.loadUrl(menuElement.path!);
  if (pop) {
    Navigator.pop(context);
  }
}

/// The app's drawer.
/// Uses the [BadenApp]s drawer config.
class BadenDrawer extends StatelessWidget {
  final bool alwaysOpen;

  const BadenDrawer({Key? key, this.alwaysOpen = false}) : super(key: key);

  /// Builds the drawer.
  @override
  Widget build(BuildContext context) {
    return NavigationDrawer(
      backgroundColor: Colors.white,
      elevation: 0,
      selectedIndex: -1,
      onDestinationSelected: (index) => onDestinationSelected(
        context,
        index,
        !alwaysOpen,
      ),
      children: [
        SizedBox(
          /// Height gotten from [DrawerHeader] code.
          height: MediaQuery.of(context).padding.top + 160,
          child: Padding(
            padding: const EdgeInsets.all(BadenLayout.defaultPadding),
            child: SvgPicture.asset(
              Assets.graphicsLogo,
            ),
          ),
        ),
        ...BadenApp.drawerConfig!.menuConfig.map((e) => e.buildWidget()),
        if (kDebugMode)
          const NavigationDrawerDestination(
            icon: Icon(Symbols.bug_report),
            label: Text("Debug"),
          ),
      ],
    );
  }
}
