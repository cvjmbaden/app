// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppConfig _$AppConfigFromJson(Map<String, dynamic> json) => AppConfig(
      initialUrl: json['initialUrl'] as String,
      menuConfig: (json['menuConfig'] as List<dynamic>)
          .map((e) => MenuElement.fromJson(e as Map<String, dynamic>))
          .toList(),
      allowedHosts: (json['allowedHosts'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      privacyPolicyLink: json['privacyPolicyLink'] as String,
      imprintLink: json['imprintLink'] as String,
    );

Map<String, dynamic> _$AppConfigToJson(AppConfig instance) {
  final val = <String, dynamic>{
    'menuConfig': instance.menuConfig.map((e) => e.toJson()).toList(),
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('allowedHosts', instance.allowedHosts);
  val['initialUrl'] = instance.initialUrl;
  val['privacyPolicyLink'] = instance.privacyPolicyLink;
  val['imprintLink'] = instance.imprintLink;
  return val;
}
