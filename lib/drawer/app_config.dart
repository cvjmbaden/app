import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'menu_element.dart';

part 'app_config.g.dart';

/// The current config's version, update when the config is changed
const configVersion = 1;

/// The url where to find the config
const configUrl = "https://www.cvjmbaden.de/app/config.json?v=$configVersion";

@JsonSerializable(explicitToJson: true, includeIfNull: false)
class AppConfig {
  final List<MenuElement> menuConfig;
  final List<String>? allowedHosts;

  final String initialUrl;
  final String privacyPolicyLink;
  final String imprintLink;

  const AppConfig(
      {required this.initialUrl,
      required this.menuConfig,
      required this.allowedHosts,
      required this.privacyPolicyLink,
      required this.imprintLink});

  /// Builds a [AppConfig] from a JSON-Like Map
  factory AppConfig.fromJson(Map<String, dynamic> json) =>
      _$AppConfigFromJson(json);

  /// Builds a [AppConfig] from a JSON String
  factory AppConfig.fromJsonString(String s) =>
      AppConfig.fromJson(jsonDecode(s));

  /// Builds a JSON-like map representing this [AppConfig]
  Map<String, dynamic> toJson() => _$AppConfigToJson(this);

  /// Builds a JSON String representing this [AppConfig]
  String toJsonString() => jsonEncode(toJson());

  bool validate() {
    return !menuConfig.any((element) => !element.validate());
  }

  MenuElement getMenuEntryWithoutDividers(int index) {
    int current = 0;
    for (final el in menuConfig) {
      if (el.type == MenuElementType.divider) continue;
      if (current == index) return el;
      current++;
    }
    throw IndexError.withLength(index, current + 1);
  }
}
