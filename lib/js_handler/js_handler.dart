import 'package:flutter_inappwebview/flutter_inappwebview.dart';

/// Utility class to easily create a new javascript handler
class JSHandler {
  final String handlerName;
  final JavaScriptHandlerCallback callback;

  const JSHandler({required this.handlerName, required this.callback});
}

/// List of JS Handlers, add handler here for easy implementation
final jsHandlers = [];

void registerJSHandlers(InAppWebViewController controller) {
  for (var handler in jsHandlers) {
    controller.addJavaScriptHandler(
      handlerName: handler.handlerName,
      callback: handler.callback,
    );
  }
}
