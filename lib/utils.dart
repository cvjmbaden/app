import 'dart:developer';

import 'package:cvjm_baden_app/drawer/app_config.dart';
import 'package:cvjm_baden_app/generated/assets.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart';

/// Calculates the String text size in the current [BuildContext] given a [TextStyle]
Size calcTextSize(BuildContext context, String text, TextStyle style) {
  final TextPainter textPainter = TextPainter(
    text: TextSpan(text: text, style: style),
    textDirection: TextDirection.ltr,
    textScaleFactor: View.of(context).platformDispatcher.textScaleFactor,
  )..layout();
  return textPainter.size;
}

/// String utilities
extension StringUtil on String {
  /// Filter will be replaced with replacemet, if String starts with filter
  String replaceStart(String filter, String replacement) {
    if (!startsWith(filter)) return this;
    return replaceFirst(filter, replacement);
  }
}

Future<void> loadConfig(String src) async {
  try {
    final config = AppConfig.fromJsonString(src);
    if (!config.validate()) {
      throw Exception(
          "Couldn't validate config -> reverting to last working config");
    }
    BadenApp.drawerConfig = config;

    final box = await Hive.openBox("appConfig");
    box.put("lastValidConfig", src);
    box.close();
  } catch (e) {
    log(e.toString());
    final box = await Hive.openBox("appConfig");
    final val = box.get("lastValidConfig",
        defaultValue:
            await PlatformAssetBundle().loadString(Assets.assetsExampleConfig));
    final config = AppConfig.fromJsonString(val);
    BadenApp.drawerConfig = config;
    box.close();
  }
}

Future<void> loadConfigFromServer() async {
  final Map<String, String> reqHeaders = {
    "User-Agent": userAgent,
  };

  if (fcmToken != null) reqHeaders["FCM-Token"] = fcmToken!;

  try {
    final res = await get(Uri.parse(configUrl), headers: reqHeaders);
    await loadConfig(res.body);
  } catch (_) {
    // loads default config from assets
    await loadConfig("");
  }
}
