import 'package:cvjm_baden_app/firebase_options.dart';
import 'package:cvjm_baden_app/main.dart';
import 'package:cvjm_baden_app/widgets/baden_web_view_widget.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

@pragma('vm:entry-point')
Future<void> firebaseBackgroundMessagingHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

/// Initializes FCM (Firebase Cloud Messaging)
Future<void> initializeFirebaseNotifications() async {
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  final messaging = FirebaseMessaging.instance;
  final notificationSettings = await messaging.requestPermission(
    alert: true,
    announcement: true,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: false,
  );

  // If permission isn't granted, do not register a background service
  if (notificationSettings.authorizationStatus ==
          AuthorizationStatus.authorized ||
      notificationSettings.authorizationStatus ==
          AuthorizationStatus.provisional) {
    FirebaseMessaging.onBackgroundMessage(firebaseBackgroundMessagingHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(
      firebaseHandleMessageInteraction,
    );
  }
}

Future<bool> tryFCMInitialization() async {
  try {
    await initializeFirebaseNotifications();
    fcmToken = await FirebaseMessaging.instance.getToken();
    return true;
  } catch (_) {
    return false;
  }
}
