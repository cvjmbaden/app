# CVJM Baden App

The official App for [CVJM Landesverband Baden e.V.](https://www.cvjmbaden.de)

## [Wiki](https://gitlab.com/cvjmbaden/app/-/wikis/home)

## Minimum mobile specs

### Android

- Android 6

### iOS

- iOS 12

## l10n
[Flutter Localization Docs](https://docs.flutter.dev/accessibility-and-localization/internationalization)

The native part of this app can be localized. This might not apply to the content displayed inside the app's webview.

All localization files are `.arb` and placed at [lib/l10n](./lib/l10n).
The corresponding code files are generated automatically at compilation or manually using `flutter gen-l10n`.
`Hot Restart` and `Hot Reload` (should) work out of the box.


### Currently supported languages
* `de`
* `en`

## Useful commands

### Code generation
[build_runner pub.dev](https://pub.dev/packages/build_runner)
```bash
dart run build_runner build
```

### Updating the app icon

Location: `assets/graphics/logo.png`

```bash
dart run flutter_launcher_icons
```

## Building the App

### Android

```bash
flutter build appbundle --no-tree-shake-icons
```

### iOS

```bash
flutter build ipa --no-tree-shake-icons
```

# ToDo

- Configure FCM for iOS

# Troubleshooting

### Build error: package `flutter_gen` not recognized
Run `flutter clean && flutter run`.
