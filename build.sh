#!/bin/bash

# Build iOS file
flutter build ipa --no-tree-shake-icons

# Build Android app bundle
flutter build appbundle --no-tree-shake-icons
